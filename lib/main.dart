import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProphecy());
}

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.lightBlueAccent,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade800,
      ),
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black12,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade100,
      ),
    );
  }
}

class ContactProphecy extends StatefulWidget {
  @override
  State<ContactProphecy> createState() => _ContactProphecyState();
}

class _ContactProphecyState extends State<ContactProphecy> {

  var currenTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currenTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: appBarMain(),
        body: bodyMain(),
      ),
    );
  }

  bodyMain(){
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              //Height constraint at Container widget level
              height: 250,
              child: Image.network(
                "https://media.discordapp.net/attachments/793093052797419520/1059816583263957112/image.png",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 68,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,

                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Chiang Mai (เชียงใหม่)",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.lightBlueAccent,
            ),
            Container(
                margin: const EdgeInsets.only(top: 8, bottom: 8),
                child: Theme(
                    data: ThemeData(
                      iconTheme: IconThemeData(
                        color: Colors.lightBlue,
                      ),
                    ),
                    child: profileActionItem()
                )
            ),
            Divider(
              color: Colors.lightBlue,

            ),
            tonightListfile(),
            Divider(
              color: Colors.lightBlue,
            ),
            tomorrowListfile2(),
          ],
        ),
      ],
    );
  }

  Widget profileActionItem(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildSunnyButton(),
        buildCloudButton(),
        buildRainButton(),
        buildMoistureButton(),
        buildWindButton(),
      ],
    );
  }

  Widget buildSunnyButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.wb_sunny_sharp,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("9 ℃"),
      ],
    );
  }

  Widget buildCloudButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.cloud,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("มีเมฆบางส่วน"),
      ],
    );
  }

  Widget buildRainButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.cloudy_snowing,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("0%"),
      ],
    );
  }

  Widget buildMoistureButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.thermostat,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("59%"),
      ],
    );
  }

  Widget buildWindButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.wind_power,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("13 กม./ชม."),
      ],
    );
  }
}

Widget tonightListfile(){
  return ListTile(
    leading: Icon(Icons.shield_moon_sharp),
    title: Text("คืนนี้"),
    trailing: Text("8 ℃   "),
  );
}

Widget tomorrowListfile2(){
  return ListTile(
    leading: Icon(Icons.sunny),
    title: Text("พรุ่งนี้"),
    trailing: Text("12 ℃   "),
  );
}
appBarMain(){
  return AppBar(
    //backgroundColor: Colors.lightBlueAccent,
    title: Text("Prophecy"),
    leading: Icon(
      Icons.arrow_back,
      //color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.star_border),
        //color: Colors.black,
        onPressed: (){
          print("Contract is starred");
        },
      ),
    ],
  );
}